# Índice

1. [Introducción](#1-introducción)
    1. [Descripción del proyecto](#11-descripción-del-proyecto)
    2. [Escenario](#12-escenario)
2. [Qué es OpenShift](#2-qué-es-openshift)
    1. [Componentes](#21-componentes)
        * [Web Console](#web-console)
        * [Container Registry](#container-registry)
        * [Proyectos y usuarios](#proyectos-y-usuarios)
        * [Builds (S2I) and Image Stream](#builds-and-image-stream)
        * [Router](#router)
        * [Service Catalog](#service-catalog)
        * [Source to Image](#source-to-image-s2i)
3. [Instalación](#3-instalación)
    1. [Proceso de instalación](#31-proceso-de-instalación)
    2. [Parámetros destacados de los inventarios](#32-parámetros-destacados-de-los-inventarios)
    2. [Comprobación de la instalación](#33-comprobación-de-la-instalación)
4. [Servidor LDAP](#4-servidor-ldap)
    1. [Sincronización con LDAP](#41-sincronización-con-ldap)
5. [Conclusión](#5-conclusión)
6. [Siguientes pasos](#6-siguientes-pasos)
7. [Webgrafía](#7-webgrafía)

# 1. Introducción

## 1.1. Descripción del proyecto

En éste proyecto se desplegará un clúster de **OpenShift**, para poder lanzar aplicaciones y servicios sobre él, en **OpenStack**, cuyos volúmenes los proporcionará el componente **Cinder** de **OpenStack** y los usuarios se tomarán de un servidor **LDAP**.

## 1.2. Escenario

![Escenario](/images/arquitectura.png)

# 2. ¿Qué es OpenShift?

OpenShift es un **PaaS** que, haciendo uso de **kubernetes**, ([Introduciendo Kubernetes](https://davtincas.wordpress.com/2018/05/07/introduciendo-kubernetes/)),  permite desplegar, y escalar, fácilmente aplicaciones en contenedores, creando una capa de abstracción para los desarrolladores. **OpenShift 3** sale en su versión 1.0 en junio de 2013 y rompe con lo que hasta entonces había sido la versión **2**.

## 2.1. Componentes

Vamos a detallar los principales componentes de OpenShift.

### Web Console

Está alojada en máster del clúster y es la interfaz web accesible para los usuarios del clúster (especialmente para los desarrolladores), permitiendo ver y gestionar el contenido de sus proyectos.

### Container Registry

Openshift ofrece la posibilidad de utilizar cualquier registro que esté implementado bajo la api de **Docker**, además de integrar un registro propio (**OpenShift Container Registry**).

### Proyectos y usuarios

El concepto de usuario, en OpenShift, no cambia al de cualquier sistema, tendremos los usuarios normales, que son creados en su primer acceso al clúster (ya sea por web o por cli) o directamente desde cli, también tenemos los usuarios de sistema, muchos de ellos creados automáticamente cuando se despliega el clúster, y por último los "service accounts" que son usuarios especiales de sistema creados automáticamente cuando se crea un proyecto y están asociados a dicho proyecto.

El concepto de proyecto es una extensión de los **namespaces** de **kubernetes**, permitiendo a los usuarios organizar y controlar sus aplicaciones sin que interfieran con las de los demás usuarios. OpenShift crea por defecto un cierto número de proyectos, pero hay que hacer especial alusión, por lo que implica a los usuarios finales, al proyecto **openshift** que es el que alojará templates e imágenes para ser usados directamente.
Cada proyecto se compone de:

* Objets: Pods, servicios, replication controllers,
* Policies: Aplicadas a los usuarios de dicho proyecto para definir **quién** puede hacer **qué** sobre los objetos.
* Constraints: Son las cuotas para limitar cada tipo de objeto.
* Service Accounts: Aplicadas a los pods de dicho proyecto para definir **qué** pueden hacer sobre los objetos.

### Builds and Image Stream

Por defecto, **OpenShift** incorpora las más que conocidas **Docker builds** y, algo que da mucha potencia al despliegue de aplicaciones, **Source to Image** (S2I). Con ésta última herramienta mencionada, es posible construir automáticamente imágenes **Docker** preparadas para desplegar, esto lo hace inyectando el código de nuestra aplicación en una imagen de un contenedor **Docker** y ensamblando una nueva imagen. La desarrollaremos más adelante.

**Image Stream** es una capa por encima de las imágenes **Docker**, permitiendo ver qué imágenes son las que están disponibles y asegurar que se está utilizando la imagen específica que necesita (incluso si la imagen del repositorio cambia). Las Builds y los Deployments son configurables para actuar si una nueva versión de una imagen está disponible, por ejemplo actualizando el despliegue. 

### Router

**OpenShift** usa el sistema de redes para pods que ofrece **Kubernetes**, asociando a cada pod una dirección ip y **SkyDNS** en el máster para la resolución de nombre de los servicios entre los pods. Pero además incorpora los **routers** que permite, acceder a los servicios desplegado a través de un nombre único, asociando al servicio una **ruta** (un nombre único) con, por ejemplo, una registro wildcard en el dns externo que apunte al nodo (normalmente nodo de infraestructuras) en el que está desplegado el router.

### Service Catalog

El service catalog son plantillas de servicios listas para desplegar en openshift, desde el Service Catalog puedes desplegar servicios como MySQL, Jenkins con integración de pipelines o crear imágenes a medida (con **S2I**) de tu software con Apache, Jboss, WildFly...

### Source to Image (S2I)

Aquí llegamos a uno de los componentes estrella de **OpenShift**, gracias a **S2I** es por lo que los usuarios pueden, simplemente, escoger del catálogo un entorno, por ejemplo **Python**, indicarle cual es su repositorio, **GitLab** por ejemplo y ya tendrá su aplicación desplegada en un contenedor **Docker** y orquestada por **Kubernetes**. Aunque parezca magia, no son más que una serie de scripts dentro de una imagen **Docker** previamente preparada, que será nuestro *builder* y éste builder, será el encargado de construirnos una imagen lista para su ejecución. 
Source to image define cuatro scripts, de los cuales dos son obligatorios:

* **Assemble**: es el script encargado de construir el artefacto a partir de nuestro código fuente.
* **Run**: ejecutará el artefacto generado.
* **Save-artifacts** (opcional): utilizará el artefacto de la construcción anterior para la siguiente incremental.
* **Usage** (opcional): información de uso.

# 3. Instalación

## 3.1 Proceso de instalación
Para realizar la instalación de OpenShift sobre OpenStack trabajaremos sobre un entorno virtual de python con las siguientes dependencias instaladas:

* [Ansible](https://pypi.python.org/pypi/ansible) version >= 2.4.1
* [Jinja2](http://jinja.pocoo.org/docs/2.9/) version >= 2.10
* [Shade](https://pypi.python.org/pypi/shade) version >= 1.26
* Python-[jmespath](https://pypi.python.org/pypi/jmespath)
* Python-[dns](https://pypi.python.org/pypi/dnspython)
* Python-[openstackclient](https://pypi.org/project/python-openstackclient/)
* Python-[heatclient](https://pypi.org/project/python-heatclient/)

Entonces clonamos el repositorio de OpenShift Origin, activamos la rama de la versión 3.9 y creamos el directorio que funcionará como inventario.
~~~
git clone https://github.com/openshift/openshift-ansible.git
cd openshift-ansible
git checkout release-3.9
mkdir -p inventory/group_vars
~~~
Dentro de **group_vars** crearemos dos yaml que definirán nuestro entorno y la configuración de nuestro clúster. 
Del fichero [**all.yml**](https://gitlab.com/DavidTinoco/OpenshiftOverOpenstack/blob/master/Provision/all.yml) tomará los valores para la pila de **OpenStack Heat** Y del fichero [**OSEv3.yml**](https://gitlab.com/DavidTinoco/OpenshiftOverOpenstack/blob/master/Provision/OSEv3.yml) se tomará la configuración del clúster y donde definiremos la configuración de acceso a nuestro **OpenStack**.

Más adelante se detallarán algunos de estos parámetro un poco más en profundidad.

Ahora, desde nuestro proyecto de OpenStack crearemos un volumen igual al tamaño que hemos definido en el inventario OSEv3.yml y definimos el ID del volumen en la variable `openshift_hosted_registry_storage_openstack_volumeID`.

Procedemos, entonces a la comprobación de los prerrequisitos y al aprovisionamiento de las instancias de OpenStack desde el propio playbook.
~~~
ansible-playbook -i ~/openshift-ansible/inventory \ 
-i ~/openshift-ansible/playbooks/openstack/inventory.py \
~/openshift-ansible/playbooks/openstack/openshift-cluster/prerequisites.yml
ansible-playbook -i ~/openshift-ansible/inventory \ 
-i ~/openshift-ansible/playbooks/openstack/inventory.py \
~/openshift-ansible/playbooks/openstack/openshift-cluster/provision.yml
~~~
Una vez estén las instancias creadas añadiremos a nuestro servidor DNS los [registros](https://gitlab.com/DavidTinoco/OpenshiftOverOpenstack/blob/master/DNS/os.pro) de cada máquina, y recargamos la zona.

Necesitaremos añadir a cada instancia el certificado de nuestro Horizon y modificar su hostname eliminando el “.novalocal” que añade OpenStack al crear la instancia.

~~~
scp ~/openshift/gonzalonazareno.crt openshift@IPFlotante:~/
ssh openshift@IPFlotante
sudo su -
update-ca-trust enable
cp /home/openshift/gonzalonazareno.crt /etc/pki/ca-trust/source/anchors/
update-ca-trust extract
hostnamectl set-hostname `hostname --short`.opens.pro
~~~

## 3.2 Parámetros destacados de los inventarios
Valores a destacar en el inventario **OSEv3.yml**

`openshift_master_default_subdomain: "apps.opens.pro"`

Es el dominio que usarán, por defecto, las aplicaciones que despleguemos sobre OpenShift, necesitaremos de un registro wildcard en nuestro dns que apunte hacia nuestro nodo de infraestructuras.

`openshift_master_cluster_public_hostname: "openshift.opens.pro"`

Éste será el nombre desde el cuál será accesible desde fuera de nuestro proyecto el panel web, deberemos tener un registro en nuestro dns que apunte hacia nuestro máster

`openshift_disable_check: disk_availability,memory_availability,docker_storage` 

Ésto sólo será necesario si el escenario que vamos a montar no cumple con los requisitos mínimos de instalación de OpenShift

`openshift_hosted_registry_storage_openstack_volumeID: 4528-2-8dsd8-sds45` 

Es necesario crear, previamente a iniciar la instalación, un volumen para el registro de OpenShift y plasmarlo aquí.

Valores a destacar en el inventario **all.yml**

`openshift_openstack_dns_nameservers: ["172.22.200.136"]` 

Es necesario que el DNS lo administremos nosotros, ya que será necesario añadir registros para que, tanto la instalación, como el acceso a los servicios, funcionen correctamente.

`openshift_openstack_subnet_cidr: "10.0.1.0/24"` 
`openshift_openstack_pool_start: "10.0.1.2"` 
`openshift_openstack_pool_end: "10.0.1.240"`

Ésta red la creará automáticamente en el aprovisionamiento del escenario, y estará conectada, mediante un router, también creado en la misma pila a nuestra ext-net.

`ansible_user: openshift`
`openshift_openstack_user: openshift`

Importante que estos dos valores coincidan, ya que será con el usuario que se realizará la instalación, no tiene por qué ser el usuario de la imagen, ya que lo crea automáticamente en el aprovisionamiento de instancias.

## 3.3 Comprobación de la instalación

Una vez haya acabado la instalación con nuestra receta de **Ansible** deberíamos poder acceder a la consola web con la dirección que le pusimos como pública, **https://openshit.opens.pro:8443** en la que podremos acceder, antes de haber configurado ningún método de autenticación, con cualquier usuario y contraseña.
![weblogin](/images/webconsolelogin.png)

Si accedemos al nodo **master** podremos ver como cada uno de los nodos que componen el clúster, los pods que se están ejecutando, los proyectos creados, servicios etc. Todo ello con la herramienta **oc**.

~~~
[openshift@master-0 ~]$ oc get nodes
NAME                     STATUS    ROLES     AGE       VERSION
app-node-0.opens.pro     Ready     compute   8d        v1.9.1+a0ce1bc657
app-node-1.opens.pro     Ready     compute   8d        v1.9.1+a0ce1bc657
infra-node-0.opens.pro   Ready     <none>    8d        v1.9.1+a0ce1bc657
master-0.opens.pro       Ready     master    8d        v1.9.1+a0ce1bc657

[root@master-0 openshift]# oc get pods --all-namespaces -o wide
NAMESPACE                           NAME                          READY     STATUS    RESTARTS   AGE       IP            NODE
default                             docker-registry-1-xxqjk       1/1       Running   0          8d        10.130.0.4    infra-node-0.opens.pro
default                             registry-console-1-k49mf      1/1       Running   0          8d        10.130.0.5    infra-node-0.opens.pro
default                             router-1-r5jdr                1/1       Running   0          8d        10.0.1.7      infra-node-0.opens.pro
kube-service-catalog                apiserver-nzbhq               1/1       Running   0          8d        10.129.0.4    master-0.opens.pro
kube-service-catalog                controller-manager-zx4qf      1/1       Running   2          8d        10.129.0.5    master-0.opens.pro
openshift-ansible-service-broker    asb-1-hkg4k                   1/1       Running   9          8d        10.130.0.8    infra-node-0.opens.pro
openshift-ansible-service-broker    asb-etcd-1-6hrbf              1/1       Running   0          8d        10.130.0.10   infra-node-0.opens.pro
openshift-template-service-broker   apiserver-5khj4               1/1       Running   0          8d        10.130.0.9    infra-node-0.opens.pro
openshift-web-console               webconsole-84466b9d97-n8ljd   1/1       Running   0          8d        10.129.0.3    master-0.opens.pro

[openshift@master-0 ~]$ oc get svc --all-namespaces
NAMESPACE                           NAME               TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)                   AGE
default                             docker-registry    ClusterIP   172.30.163.68    <none>        5000/TCP                  8d
default                             kubernetes         ClusterIP   172.30.0.1       <none>        443/TCP,53/UDP,53/TCP     8d
default                             registry-console   ClusterIP   172.30.235.180   <none>        9000/TCP                  8d
default                             router             ClusterIP   172.30.156.95    <none>        80/TCP,443/TCP,1936/TCP   8d
kube-service-catalog                apiserver          ClusterIP   172.30.163.124   <none>        443/TCP                   8d
openshift-ansible-service-broker    asb                ClusterIP   172.30.55.190    <none>        1338/TCP                  8d
openshift-ansible-service-broker    asb-etcd           ClusterIP   172.30.66.116    <none>        2379/TCP                  8d
openshift-template-service-broker   apiserver          ClusterIP   172.30.252.249   <none>        443/TCP                   8d
openshift-web-console               webconsole         ClusterIP   172.30.236.120   <none>        443/TCP                   8d

[openshift@master-0 ~]$ oc get pvc --all-namespaces
NAMESPACE                          NAME             STATUS    VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
default                            registry-claim   Bound     registry-volume                            5Gi        RWO                           8d
openshift-ansible-service-broker   etcd             Bound     pvc-93ff5489-665e-11e8-979a-fa163ec5a8ed   1Gi        RWO            standard       8d

[openshift@master-0 ~]$ oc get project 
NAME                                DISPLAY NAME               STATUS
default                                                        Active
kube-public                                                    Active
kube-service-catalog                                           Active
kube-system                                                    Active
logging                                                        Active
management-infra                                               Active
openshift                                                      Active
openshift-ansible-service-broker                               Active
openshift-infra                                                Active
openshift-node                                                 Active
openshift-template-service-broker                              Active
openshift-web-console                                          Active
~~~

# 4. Servidor LDAP

Para la autenticación de los usuarios, usaremos un servidor LDAP que previamente tenemos configurado, estando en el mismo dominio (opens.pro) y accesible a través de ipflotante, además de estar configurado en el dns. En dicho servidor, vamos a tener creado las unidades [organizativas](https://gitlab.com/DavidTinoco/OpenshiftOverOpenstack/blob/master/LDAP/unidadesorganizativas.ldif), un [grupo](https://gitlab.com/DavidTinoco/OpenshiftOverOpenstack/blob/master/LDAP/group.ldif) y dos usuarios, [david.tinoco](https://gitlab.com/DavidTinoco/OpenshiftOverOpenstack/blob/master/LDAP/david.tinoco.ldif) y [misael.noda](https://gitlab.com/DavidTinoco/OpenshiftOverOpenstack/blob/master/LDAP/misael.noda.ldif), cuyas contraseñas estarán generadas por la herramienta **slappasswd**.

## 4.1 Sincronización con LDAP

OpenShift tiene distintas formas de autenticación, cada una de ellas está definida por un **[identityProvider](https://docs.openshift.org/3.9/install_config/configuring_authentication.html#identity-providers)**. OpenShift incluye un servidor OAuth que hace uso del identity provider para determinar quién tiene acceso al sistema. Los roles y permisos se les añadirán luego, desde el usuario **system:admin** con las herramientas **oc policy** y **oc adm policy**.

Entonces modificaremos el yaml de configuración del máster, para indicarle que nuestro identity provider será `LDAPPasswordIdentityProvider`, para ello modificaremos el fichero **/etc/origin/master/master-config.yaml** y modificamos la parte de la autenticación, para que quede como podemos ver [aquí](https://gitlab.com/DavidTinoco/OpenshiftOverOpenstack/blob/master/OpenShift/master-config.yaml). Y entonces reiniciamos los servicios.
~~~
systemctl restart origin-master-api.service origin-master-controllers origin-node.service
~~~
# 5. Conclusión

**OpenShift** está llamado a ser uno de los grandes PaaS del mercado, ofrece una versatilidad enorme y se puede hacer todo aquello a lo que tu imaginación te lleve, perfecto para **integración continua** y **despliegue continuo**. Poco a poco se ha ido acercando a su competidor más cercano, y líder PaaS **Heroku** . Aquí podemos ver una gráfica desde el día en que se lanzó **OpenShift v3 Origin** hasta el día de hoy, donde se aprecia cómo está cogiendo fuerza e impulso **OpenShift** y otros PaaS como **Jelastic** siguen siendo insignificantes en el mercado.

![leyenda](/images/leyenda.png)
![grafica](/images/grafica.png)

# 6. Siguientes pasos

* Desplegarlo con masters y nodos de infraestructuras en alta disponibilidad
* Desarrollo de templates para el Service Catalog
* Desarrollo de builders S2I para aplicaciones que requiramos
* Y mucho, mucho más

# 7. Webgrafía

Toda el proyecto se ha desarrollado basándose en la documentación oficial de **OpenShift Origin**, que la podemos encontrar [aquí](https://docs.openshift.org/) y también se ha consultado, para la herramienta **S2I**, un blog de la **paradigmadigital**, [ésta](https://www.paradigmadigital.com/dev/como-generar-dockers-de-forma-sencilla-con-s2i/) en concreto. 